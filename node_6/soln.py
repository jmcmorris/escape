#!/usr/bin/python

import json
import os
import pprint
import sys

SCRIPT_PATH = lambda *pieces: os.path.abspath(os.path.join(os.path.dirname(__file__), *pieces))
sys.path.append(SCRIPT_PATH("..", "external", "requests"))
sys.path.append(SCRIPT_PATH("..", "external", "matplotlib", "lib"))
sys.path.append(SCRIPT_PATH("..", "external", "dateutil"))
sys.path.append(SCRIPT_PATH("..", "external", "pyparsing"))

import requests
import matplotlib
import matplotlib.pyplot as plt

r = requests.get("https://64.129.254.38:8000/api/?session=399cbcd7-7ddc-4495-b9f3-de5343927256&command=longrange", verify=False)

matplotlib.rcParams['axes.unicode_minus'] = False
fig = plt.figure()

ax = fig.add_subplot(111)
ax.set_title('Long Range Scanner')
result = json.loads(r.content)
for star in result['stars']:
  x = int(star['x'])
  y = int(star['y'])
  ax.plot([x], [y], 'ro')
  plt.annotate(star['name'], xy=(x, y), xytext=(x, y), arrowprops=dict(facecolor='black', shrink=0.05))

r = requests.get("https://64.129.254.38:8000/api/?session=399cbcd7-7ddc-4495-b9f3-de5343927256&command=ship&arg=show", verify=False)
result = json.loads(r.content)
x = result['unix']
x = result['uniy']
ax.plot([x], [y], 'o')
plt.annotate(star['name'], xy=(x, y), xytext=(x, y), arrowprops=dict(facecolor='black', shrink=0.05))

plt.show()
