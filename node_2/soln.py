#!/usr/bin/python

import json
import sys
import pprint
sys.path.append("../external/requests")

import requests

r = requests.get("https://64.129.254.38:8000/api/?session=399cbcd7-7ddc-4495-b9f3-de5343927256&command=ship&arg=showsystems", verify=False)

for item in json.loads(r.content)['systems']:
    print item['name'],':', item['functionality']

