#!/usr/bin/python

import json
import os
import pprint
import sys
import time

SCRIPT_PATH = lambda *pieces: os.path.abspath(os.path.join(os.path.dirname(__file__), *pieces))
sys.path.append(SCRIPT_PATH("..", "external", "requests"))

import requests
import matplotlib
import matplotlib.pyplot as plt
from numpy import array

def api(command, *args):
  parms = '&'.join('arg%s=%s' % (str(i+1) if i else '', val) for i, val in enumerate(args))
  if parms:
    parms = '&'+parms
  url = "https://64.129.254.38:8000/api/?session=399cbcd7-7ddc-4495-b9f3-de5343927256&command=%s%s" % (command, parms)
  #print url
  r = requests.get(url, verify=False)
  return json.loads(r.content)

def get_ship_pos():
  result = api('ship', 'show')
  x = result['unix']
  y = result['uniy']
  return array((x, y))

def get_current_system():
  result = api('ship', 'show')
  return result['currentsystem']

def get_stars():
  stars = {}
  result = api('longrange')
  print result
  for star in result['stars']:
    x = int(star['x'])
    y = int(star['y'])
    name = star['name']
    stars[name] = array((x, y))
  return stars

def get_dest():
  print "Enter destination system."
  dest = sys.stdin.readline()
  print "dest is ", dest
  return dest.strip()

def set_ship_vector(x, y):
  api('ship', 'setunivecx', x)
  api('ship', 'setunivecy', y)
  print "setting vector to: ", x, y

def plot_map(stars, ship_pos):
  matplotlib.rcParams['axes.unicode_minus'] = False
  fig = plt.figure()

  ax = fig.add_subplot(111)
  ax.set_title('Long Range Scanner')
  for name,pos in stars.items():
    x = int(pos[0])
    y = int(pos[1])
    ax.plot(x, y, 'ro')
    plt.annotate(name, xy=(x, y), xytext=(x, y), arrowprops=dict(facecolor='black', shrink=0.05))
  ax.plot(ship_pos[0], ship_pos[1], 'o')
  plt.show()

def scale(num, limit=0.4):
  sign = 1 if num >= 0 else -1
  num = min(abs(num), limit)
  return num * sign

stars = get_stars()
ship_pos = get_ship_pos()
print "ship pos", ship_pos

set_ship_vector(0,0)

plot_map(stars, ship_pos)
dest = get_dest()
dest_pos = stars[dest]
print "going to:", stars[dest]

while get_current_system() != dest:
  ship_pos  = get_ship_pos()
  print "ship pos", ship_pos
  diff = array(dest_pos - ship_pos, dtype='f')
  diff[0] = scale(diff[0])
  diff[1] = scale(diff[1])
  set_ship_vector(diff[0], diff[1])
  time.sleep(.5)

set_ship_vector(0,0)
ship_pos = get_ship_pos()
print "ship pos", ship_pos

