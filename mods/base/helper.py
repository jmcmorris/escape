import bisect


class EventArg(object):
    def __init__(self, value):
        self.start = value
        self.final = value

def callEvent(name, player, *args, **kwargs):
    values = []
    for value in kwargs.values():
        values.append(EventArg(value))
    args = list(args) + values
    if player:
        event = player.entity.event
        if event.has(name):
            event[name].invoke(player, *args)
        game.events[name].invoke(player, *args)
    else:
        game.events[name].invoke(*args)
    return [value.final for value in values]


def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    reverse = dict((value, key) for key, value in enums.iteritems())
    enums['reverse_mapping'] = reverse
    return type('Enum', (), enums)


class WeightedRandomGenerator(object):
    def __init__(self, weights):
        self.totals = []
        total = 0
        for weight in weights:
            total += weight
            self.totals.append(total)

    def __call__(self):
        return bisect.bisect_right(self.totals, Random.getFloat() * self.totals[-1])

def shuffle(data):
    '''Randomly shuffles the provided list in place.'''
    #Used from Python random library with modification for Crea
    for i in reversed(xrange(1, len(data))):
        # pick an element in data[:i+1] with which to exchange data[i]
        j = int(Random.getFloat() * (i+1))
        data[i], data[j] = data[j], data[i]

def select(data):
    '''Randomly selects an element from data. Selected element is removed from data.'''
    length = len(data)
    if length == 1:
        value = data[0]
        del data[0]
        return value
    elif length >= 2:
        index = Random.get(len(data) - 1)
        value = data[index]
        data[index] = data[-1]
        del data[-1]
        return value
    else:
        assert False, "Cannot select on empty container."

#Copied from Python functools.partial to avoid that dependency
def partial(func, *args, **keywords):
    def newfunc(*fargs, **fkeywords):
        newkeywords = keywords.copy()
        newkeywords.update(fkeywords)
        return func(*(args + fargs), **newkeywords)
    newfunc.func = func
    newfunc.args = args
    newfunc.keywords = keywords
    return newfunc
