import json
import sys
import pprint
sys.path.append("external/requests")

import requests
from numpy import array

SCALER = 3

class Ship(object):
    @staticmethod
    def create(data):
        return Ship(data["name"], data["x"], data["y"])

    def __init__(self, name, x, y, isUs=False):
        self.name = name
        self.x = x
        self.y = y
        self.vecx = 0
        self.vecy = 0
        self.isUs = isUs

    def getPos(self):
        return array((self.x, self.y))

    def update(self):
        x = SCALER * float(self.x)
        y = SCALER * float(self.y)
        vecx = 10*SCALER * float(self.vecx)
        vecy = 10*SCALER * float(self.vecy)
        game.debugRender.add(FloatRect(x, y, 2, 2), Color(100, 200, 200))
        game.debugRender.add(Vectorf(x, y), Vectorf(x + vecx, y + vecy), Color(255, 255, 255))

    def updateData(self, data):
        self.updatePos(data['x'], data['y'])

    def updatePos(self, x, y):
        self.vecx = x - self.x
        self.vecy = y - self.y
        self.x = x
        self.y = y

    def getDest(self):
        return array((self.x + self.vecx, self.y + self.vecy))

    def render(self, window):
        self.label = DrawableText(str(self.name), "content/ui/Bitter-Regular.ttf", 16 if self.isUs else 10)
        self.label.setColor(Color(16, 16, 16))
        position = Vectorf(SCALER * float(self.x), SCALER * float(self.y))
        self.label.setPosition(position)
        self.label.render(window)

class Planet(object):
    def __init__(self, data):
        self.x = data['x']
        self.y = data['y']
        self.cloud_cover = data['cloud_cover']
        self.day = data['day']
        self.esc_velocity = data['esc_velocity']
        self.orbit_zone = data['orbit_zone']
        self.name = data['planet_no']
        self.radius = data['radius']
        self.surf_grav = data['surf_grav']
        self.type = data['type']

    def getPos(self):
        return array((self.x, self.y))

def api(command, *args):
  parms = '&'.join('arg%s=%s' % (str(i+1) if i else '', val) for i, val in enumerate(args))
  if parms:
    parms = '&'+parms
  url = "https://64.129.254.38:8000/api/?session=69211b38-eb60-48b4-93ac-8cf93b0c2129&command=%s%s" % (command, parms)
  #print url
  r = requests.get(url, verify=False)
  return json.loads(r.content)


def get_ship_data():
    return api('ship', 'show')

def get_ship_pos_inner(result, isViewingSystem):
  prefix = 'system' if isViewingSystem else 'uni'
  x = result[prefix + 'x']
  y = result[prefix + 'y']
  return array((x, y))

def get_ship_pos(isViewingSystem):
  return get_ship_pos_inner(get_ship_data(), get_ship_data)

def get_current_system():
  result = api('ship', 'show')
  return result['currentsystem']

def get_stars():
  stars = {}
  result = api('longrange')
  print result
  for star in result['stars']:
    x = int(star['x'])
    y = int(star['y'])
    name = star['name']
    stars[name] = array((x, y))
  return stars

def get_ships(scanRange):
  result = api(scanRange)
  ships = {}
  for ship in result['otherships']:
    ship = Ship.create(ship)
    ships[ship.name] = ship
  return ships

def get_planets():
    planets = {}
    result = api('shortrange')
    for data in result['system']['planetarray']:
        planet = Planet(data)
        planets[planet.name] = planet
    return planets

def escape_system(shipX, shipY):
    if abs(100 - shipX) > abs(100 - shipY):
        x = shipX
        y = 199 if shipY > 100 else 1
    else:
        x = 199 if shipX > 100 else 1
        y = shipY
    Log.info("Escaping: ({}, {})".format(x, y))
    return array((x, y))

def get_dest():
  print "Enter destination system."
  dest = sys.stdin.readline()
  print "dest is ", dest
  return dest.strip()

def set_ship_vector(x, y, isViewingSystem):
  prefix = 'system' if isViewingSystem else 'uni'
  api('ship', 'set{}vecx'.format(prefix), x)
  api('ship', 'set{}vecy'.format(prefix), y)

  Log.info("setting {}vector to: ({}, {})".format(prefix, x, y))

def scale(num, limit=1):
  limit = limit if abs(num) > 2 else 0.1
  sign = 1 if num >= 0 else -1
  num = min(abs(num), limit)
  return num * sign

class InfoUi(DialogUi):
    def __init__(self):
        DialogUi.__init__(self, 'info', Rectangle(600, 40, 206, 200))
        self.load('asset://crea/content/ui/escape.html')

class EscapeUi(Renderable):
    def __init__(self):
        Renderable.__init__(self)
        game.render.addRenderable(self, 6000)
        self.info = InfoUi()
        self.labels = []
        self.isViewingSystem = False
        self.ship = Ship("Elementalist (Us)", 0, 0, isUs=True)
        self.stars = get_stars()
        self.ships = get_ships('longrange')
        for name, data in self.stars.iteritems():
            self.labels.append(self.createLabel(name, Vectorf(SCALER * float(data[0]), SCALER * float(data[1]))))
        self.shipTimer = Timer(0)
        self.currentPlanet = "none"
        self.planets = get_planets() if self.isInSystem() else {}
        self.updateShipData()
        self.moveTimer = Timer(500)
        self.destination = ""
        self.destPos = array([])
        self.info.show()
        self.inputs = Input()
        self.mineInput = self.inputs.createAction()
        self.mineInput.inputs.append(KeyInput(Keyboard.M))
        self.mineInput.onChange.listen(self.handleMine)
        self.rangeInput = self.inputs.createAction()
        self.rangeInput.inputs.append(KeyInput(Keyboard.R))
        self.rangeInput.onChange.listen(self.handleRangeChange)
        self.escapeInput = self.inputs.createAction()
        self.escapeInput.inputs.append(KeyInput(Keyboard.E))
        self.escapeInput.onChange.listen(self.handleEscape)
        self.shutdownInput = self.inputs.createAction()
        self.shutdownInput.inputs.append(KeyInput(Keyboard.S))
        self.shutdownInput.onChange.listen(self.handleShutdown)
        self.targetInput = self.inputs.createAction()
        self.targetInput.inputs.append(KeyInput(Keyboard.T))
        self.targetInput.onChange.listen(self.handleTarget)
        self.showShipLabels = True
        self.target = None

    def updateShipData(self):
        shipData = get_ship_data()
        currentPlanet = shipData['currentplanet']
        if currentPlanet != self.currentPlanet:
            self.currentPlanet = currentPlanet
            self.updateInfo()
        shipPos = get_ship_pos_inner(shipData, self.isViewingSystem)
        self.ship.updatePos(shipPos[0], shipPos[1])
        self.shipTimer.reset(100)
        result = api("shortrange") if self.isViewingSystem else api("longrange")
        for data in result['otherships']:
            name = data["name"]
            if name in self.ships:
                self.ships[name].updateData(data)
            else:
                self.ships[name] = Ship.create(data)

    def update(self, frameTime):
        self.inputs.update(frameTime)
        self.shipTimer.update(frameTime)
        self.moveTimer.update(frameTime)
        if self.shipTimer.expired():
            self.updateShipData()

        if self.isViewingSystem:
            self.renderPlanets()
        else:
            self.renderSystems()
        self.ship.update()
        for name, ship in self.ships.iteritems():
            ship.update()
        if Mouse.isButtonPressed(Mouse.Button.Left):
            pos = game.getMousePosition()
            self.target = None
            if self.isViewingSystem:
                for name, planet in self.planets.iteritems():
                    if abs(SCALER * planet.x - pos.x) < 20 and abs(SCALER * planet.y - pos.y) < 20:
                        self.destination = name
                        self.destPos = planet.getPos()
            else:
                for name, data in self.stars.iteritems():
                    if abs(SCALER * data[0] - pos.x) < 20 and abs(SCALER * data[1] - pos.y) < 20:
                        self.destination = name
                        self.destPos = data
                        Log.info("Destination System: {}".format(name))
        if self.destPos.any() and self.moveTimer.expired():
            self.updateMovement()

    def handleMine(self, actionState):
        if actionState.justPressed:
            pass

    def handleShutdown(self, actionState):
        if actionState.justPressed:
            self.shutdownFTL()

    def handleRangeChange(self, actionState):
        if actionState.justPressed and self.isInSystem():
            self.shutdownFTL()
            #Swap current range
            self.isViewingSystem = not self.isViewingSystem
            self.labels = []
            self.destination = ""
            Log.info("Viewing System: {}".format(self.isViewingSystem))
            if self.isViewingSystem:
                self.ships = get_ships('shortrange')
                self.planets = get_planets()
                for name, planet in self.planets.iteritems():
                    Log.info("Planet: {} at ({}, {})".format(name, planet.x, planet.y))
                    self.labels.append(self.createLabel(name, Vectorf(SCALER * float(planet.x), SCALER * float(planet.y))))
            else:
                self.ships = get_ships('longrange')
                for name, data in self.stars.iteritems():
                    self.labels.append(self.createLabel(name, Vectorf(SCALER * float(data[0]), SCALER * float(data[1]))))

    def handleTarget(self, actionState):
        if actionState.justPressed:
            for name, ship in self.ships.iteritems():
                pos = game.getMousePosition()
                if abs(SCALER * ship.x - pos.x) < 20 and abs(SCALER * ship.y - pos.y) < 20:
                    self.target = ship
                    Log.info("Moving towards: {}".format(ship.name))
                    self.destPos = ship.getDest()

    def handleEscape(self, actionState):
        if actionState.justPressed:
            self.destPos = escape_system(self.ship.x, self.ship.y)

    def isInSystem(self):
        return get_current_system() != "none"

    def renderPlanets(self):
        for name, planet in self.planets.iteritems():
            game.debugRender.add(FloatRect(SCALER * float(planet.x), SCALER * float(planet.y), 2, 2), Color(255, 100, 100))

    def renderSystems(self):
        for name, data in self.stars.iteritems():
            game.debugRender.add(FloatRect(SCALER * float(data[0]), SCALER * float(data[1]), 2, 2), Color(255, 100, 100))

    def updateInfo(self):
        commands = []
        Log.info(str(self.currentPlanet))
        if self.currentPlanet and self.currentPlanet != "none":
            data = get_ship_data()
            commands.append("App.info.details.clear();")
            planet = self.planets[self.currentPlanet]
            for key, value in planet.__dict__.iteritems():
                commands.append("App.info.details.pushObject(App.Data.create({{name: '{}', value: '{}'}}));".format(key, value))
            self.info.view.execute(commands)

    def hasReachedDestination(self, diff):
        if self.isViewingSystem:
            if self.destination:
                Log.info("Planet current={}    destination={}".format(self.currentPlanet, self.destination))
                return self.destination == self.currentPlanet
            else:
                Log.info("Diff: ({}, {})".format(diff[0], diff[1]))
                return abs(diff[0] + diff[1]) < 0.0001
        else:
            if self.destination:
                current = get_current_system()
                Log.info("current={}    destination={}".format(current, self.destination))
                return current == self.destination
            else:
                Log.info("Diff: ({}, {})".format(diff[0], diff[1]))
                return abs(diff[0] + diff[1]) < 0.0001

    def updateMovement(self):
        if self.target:
            self.destPos = self.target.getDest()
        Log.info("Shippos: ({}, {})    destpos: ({}, {})".format(self.ship.x, self.ship.y, self.destPos[0], self.destPos[1]))
        self.moveTimer.reset(50)
        diff = array(self.destPos - self.ship.getPos(), dtype='f')
        if not self.hasReachedDestination(diff):
            diff[0] = scale(diff[0])
            diff[1] = scale(diff[1])
            set_ship_vector(diff[0], diff[1], self.isViewingSystem)
        else:
            self.shutdownFTL()

    def shutdownFTL(self):
        self.destPos = array([])
        set_ship_vector(0,0, self.isViewingSystem)

    def createLabel(self, text, position):
        label = DrawableText(str(text), "content/ui/Bitter-Regular.ttf", 16)
        label.setColor(Color(16, 16, 16))
        label.setPosition(position)
        return label

    def render(self, window):
        for label in self.labels:
            label.render(window)
        if self.showShipLabels:
            for ship in self.ships.values():
                ship.render(window)
        self.ship.render(window)
