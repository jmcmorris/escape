class DialogUi(object):
    '''Base dialog class that handles the dialog fundamentals.'''

    def __init__(self, viewName, viewArea):
        self.view = game.gui.createView(viewName, viewArea)

    def load(self, page):
        self.view.load(page)

    def show(self, focus=True):
        self.view.show()
        if focus:
            game.gui.focusView(self.view)

    def hide(self):
        self.view.hide()

    def toggleVisibility(self):
        self.hide() if self.view.isVisible() else self.show()
