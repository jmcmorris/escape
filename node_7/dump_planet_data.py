#!/usr/bin/python

import colorama
colorama.init()

from colorama import Fore

import json
import sys
from pprint import pprint
sys.path.append("../external/requests")

import requests

to_mine = sys.argv[1] if len(sys.argv) > 1 else None
#print "Planning to mine", to_mine

def api(command, *args):
  parms = '&'.join('arg%s=%s' % (str(i+1) if i else '', val) for i, val in enumerate(args))
  if parms:
    parms = '&'+parms
  url = "https://64.129.254.38:8000/api/?session=399cbcd7-7ddc-4495-b9f3-de5343927256&command=%s%s" % (command, parms)
  #print url
  r = requests.get(url, verify=False)
  return json.loads(r.content)

planet_stats = api('object')['object_data']
print "Planet data:"
for key, val in planet_stats.items():
    spaces = " "*(30-len(key))
    print '%s:%s%s' % (key, spaces, val)

something = False
print
if int(planet_stats['minor_moons']):
    something = True
    print Fore.BLUE + 'Chance of Xenium!'
if float(planet_stats['volatile_gas_inventory']) > 0.01:
    something = True
    print Fore.GREEN +'Chance of Vespine (%s)!' % planet_stats['volatile_gas_inventory']
if float(planet_stats['hydrosphere']) > 0.01:
    something = True
    print Fore.YELLOW + 'Chance of Tiberium (%s)!' % planet_stats['hydrosphere']

if not something:
    print Fore.RED + 'No resources at this planet :('
