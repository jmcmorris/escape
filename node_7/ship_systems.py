#!/usr/bin/python

import json
import sys
import pprint
sys.path.append("../external/requests")

import requests

to_repair = sys.argv[1] if len(sys.argv) > 1 else None
print "Planning to repair", to_repair

def api(command, *args):
  parms = '&'.join('arg%s=%s' % (str(i+1) if i else '', val) for i, val in enumerate(args))
  if parms:
    parms = '&'+parms
  url = "https://64.129.254.38:8000/api/?session=399cbcd7-7ddc-4495-b9f3-de5343927256&command=%s%s" % (command, parms)
  #print url
  r = requests.get(url, verify=False)
  return json.loads(r.content)


fab_workshop = {
    "Alcubierre Singularity Propulsion Drive": { "Unobtainium": 2},
    "Manifold Tidal Stabilizer": { "Vespene gas": 2},
    "Repulsor Shields": { "Xenium": 2},
    "Directed Energy Weapon System": {"Unobtainium": 2},
    "Mining Drones": { "Tiberium": 6},
    "Tactical Conflict AI": { "Unobtainium": 2, "Tiberium": 4},
    "Gravitational Disturbance Sensor": { "Vespene gas": 2},
    "Sublight Impulse Propulsion Drive": { "Xenium": 2},
    "Devirtualization Fab Workshop": { "Tiberium": 6}
}

resources = {}
for item in api('ship', 'showresources')['resources']:
    resources[item['name']] = item['amount']

print 'We have:',resources

health = {}
for item in api('ship', 'showsystems')['systems']:
    health[item['name']] = item['functionality']

def can_repair(reqs):
    for resource_name, val in reqs.items():
        if resources[resource_name] < val:
            return False
    return True

def needed(reqs):
  ret = {}
  for name, val in reqs.items():
    have = resources[name]
    if resources[name] < val:
      ret[name] = val - have
  return ret

for name, health in health.items():
  if health < 1:
    reqs = fab_workshop[name]
    name_health = "%s (%s)" % (name, health)
    spaces = " "*(50-len(name_health))
    if can_repair(reqs):
        print '%s:%s req:  %s' % (name_health, spaces, reqs)
        if name == to_repair:
            rep = '  Repairing'
            spaces = " "*(52-len(rep))
            print '%s:%s%s' % (rep, spaces, api('ship', 'dorepair', name))
    else:
        print '%s:%s req:  %s' % (name_health, spaces, reqs)
        print " "*52+'need: %s' % (needed(reqs),)
  else:
    spaces = " "*(50-len(name))
    print '%s:%s%s' % (name, spaces, 'OK')
  print

