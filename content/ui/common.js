
//config.setCustomCSS("::-webkit-scrollbar { width: 20px; height: 20px; } ::-webkit-scrollbar-track { background-color: blue; } ::-webkit-scrollbar-thumb { background-color: red; }");

jQuery(document).ready(function($){

    var windowTemplate = ' \
        <div class="window-container"> \
            <div id="handles" class="handle" onmousedown="Crea.startDrag();"> \
                <h2 class="tab-title"></h2> \
                <div class="close_handle" onclick="Crea.close();"></div> \
            </div> \
            <table cellspacing="0" cellpadding="0" class="window-framework"> \
                <tr class="top-row"> \
                    <td class="top-left"></td> \
                    <td class="top"></td> \
                    <td class="top-right"></td> \
                </tr> \
                <tr class="window-content"> \
                    <td class="middle-left"></td> \
                    <td class="middle content"></td> \
                    <td class="middle-right"></td> \
                </tr> \
                <tr> \
                    <td class="bottom-left"></td> \
                    <td class="bottom"></td> \
                    <td class="bottom-right"></td> \
                </tr> \
            </table> \
        </div>';

    var headerTemplate = ' \
        <tr class="header-top"> \
            <td class="top-left"></td> \
            <td class="top"></td> \
            <td class="top-right"></td> \
        </tr> \
        <tr class="header-middle"> \
            <td class="middle-left"></td> \
            <td class="middle content"></td> \
            <td class="middle-right"></td> \
        </tr> \
        <tr class="header-bottom"> \
            <td class="bottom-left"></td> \
            <td class="bottom"></td> \
            <td class="bottom-right"></td> \
        </tr>';


    $('.window').each(function(){

        var windowContainer = $(windowTemplate);

        if($(this).hasClass('with-header')) {
            var windowHeader = $(headerTemplate);
            windowContainer.find('tr.top-row').after(windowHeader).remove();
            windowHeader.find('td.content').append($(this).find('.header'));
        }

        var handle = $(this).data('handle');
        if(handle) {
            windowContainer.find('#handles .tab-title').text(handle);
        } else {
            windowContainer.find('#handles').remove();
        }

        windowContainer.insertAfter($(this)).find('tr.window-content td.content').append($(this));
        $(document).trigger('creaWindowLoaded');
    });

    jQuery.fn.tracking = function () {
        this.data('hovering', false);

        this.hover(function () {
            $(this).data('hovering', true);
        }, function () {
            $(this).data('hovering', false);
        });

        return this;
    };

    jQuery.fn.hovering = function () {
      return this.data('hovering');
    };

});