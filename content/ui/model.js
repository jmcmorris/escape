App = Ember.Application.create();

App.Item = Ember.Object.extend({
    id: 0,
    content: 0,
    name: '',
    icon: ''
});

App.ItemView = Ember.View.extend({
    content: null,
    didInsertElement: function() {
        this.$('div').tracking();
    },
    mouseEnter: function() {
        if (this.content != null && (this.content.id != 0 || this.content.content != 0)) {
            var area = getArea(this.$('div'));
            Crea.invoke('showTooltip', 'item', this.content.id, this.content.content, area);
        }
    },
    watchContent: function() {
        if (this.$('div').hovering()) {
            var area = getArea(this.$('div'));
            Crea.invoke('showTooltip', 'item', this.content.id, this.content.content, area);
        }
    }.observes('content.id', 'content.content')
});

function getArea(element) {
    var offset = element.offset();
    return { x: offset.left, y: offset.top, width: element.width(), height: element.height() };
}
