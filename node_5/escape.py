import json
import sys
import pprint
sys.path.append("external/requests")

import requests

class EscapeUi(DialogUi):
    def __init__(self):
        DialogUi.__init__(self, 'escape', Rectangle(100, 40, 206, 200))
        self.load('asset://crea/content/ui/escape.html')
        self.view.show()

    def update(self, frameTime):
        commands = []
        r = requests.get("https://64.129.254.38:8000/api/?session=399cbcd7-7ddc-4495-b9f3-de5343927256&command=ship&arg=show", verify=False)
        result = json.loads(r.content)
        for key, value in result.iteritems():
            command = "App.info.set('{}', '{}');".format(key, value)
            commands.append(command)
        self.view.execute(commands)
