#!/usr/bin/env python

import sys
from pprint import pprint
import cPickle as pickle

def try_float(x):
  try:
    return float(x)
  except ValueError:
    return x

with open('planet.db', 'rb') as f:
  data = pickle.load(f)

if len(sys.argv) <= 1:
  pprint(data)
else:
  query = sys.argv[1]
  if query[0] == '=':
    pprint(data[query[1:]])
  else:
    print 'Showing %s' % query
    for planet in sorted(data.values(), key=lambda x: try_float(x[query])):
      print '%s: %s' % (planet['planet_no'], planet[query])

