#!/usr/bin/python

import colorama
colorama.init()

from colorama import Fore

import json
import sys
from pprint import pprint
from lib import api

to_mine = sys.argv[1] if len(sys.argv) > 1 else None
print "Planning to mine", to_mine
assert(to_mine in {None, "Xenium", "Tiberium", "Vespene gas", "Unobtainium"})

planet_stats = api('object')['object_data']
print "Planet data:"
for key, val in planet_stats.items():
    spaces = " "*(30-len(key))
    print '%s:%s%s' % (key, spaces, val)

something = False
print
if int(planet_stats['minor_moons']):
    something = True
    print Fore.BLUE + 'Chance of Xenium (%s)!' % planet_stats['minor_moons']
if float(planet_stats['volatile_gas_inventory']) > 0.01:
    something = True
    print Fore.GREEN +'Chance of Vespene gas (%s)!' % planet_stats['volatile_gas_inventory']
if float(planet_stats['hydrosphere']) > 0.01:
    something = True
    print Fore.YELLOW + 'Chance of Tiberium (%s)!' % planet_stats['hydrosphere']

if not something:
    print Fore.RED + 'No resources at this planet :('

if to_mine != None:
    result = api('ship', 'drones', to_mine)
    print "mining result:"
    pprint(result)
