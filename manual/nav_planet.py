#!/usr/bin/python

import json
import os
import pprint
import sys
import time

#import signal
#signal.signal(signal.SIGINT, lambda *sig: set_ship_vector(0,0))

from lib import api

import matplotlib
import matplotlib.pyplot as plt
from numpy import array

def get_ship_pos():
  result = api('ship', 'show')
  x = result['systemx']
  y = result['systemy']
  return array((x, y))

def get_current_planet():
  result = api('ship', 'show')
  return result['currentplanet']

def get_planets():
  planets = {}
  result = api('shortrange')
  print result
  for planet in result['system']['planetarray']:
    x = int(planet['x'])
    y = int(planet['y'])
    name = planet['planet_no']
    planets[name] = array((x, y))
  return planets

def get_dest():
  print "Enter destination planet."
  dest = sys.stdin.readline()
  print "dest is ", dest
  return dest.strip()

def set_ship_vector(x, y):
  api('ship', 'setsystemvecx', x)
  api('ship', 'setsystemvecy', y)
  print "setting vector to: ", x, y

def plot_map(planets, ship_pos):
  matplotlib.rcParams['axes.unicode_minus'] = False
  fig = plt.figure()

  ax = fig.add_subplot(111)
  ax.set_title('Short Range Scanner')
  for name,pos in planets.items():
    x = int(pos[0])
    y = int(pos[1])
    ax.plot(x, y, 'ro')
    plt.annotate(name, xy=(x, y), xytext=(x, y), arrowprops=dict(facecolor='black', shrink=0.05))
  ax.plot(ship_pos[0], ship_pos[1], 'o')
  plt.show()

def scale(num, limit=5):
  limit=5 if abs(num) > 2 else 0.1
  sign = 1 if num >= 0 else -1
  num = min(abs(num), limit)
  return num * sign

def go_to_pos(dest_pos):
  print "going to:", dest_pos

  ship_pos = get_ship_pos()
  diff = array(dest_pos - ship_pos, dtype='f')
  print "diff is:", diff
  while abs(diff[0]) > 2 or abs(diff[1]) > 2:
    print "moving to position"
    ship_pos  = get_ship_pos()
    print "ship pos", ship_pos
    diff = array(dest_pos - ship_pos, dtype='f')
    diff[0] = scale(diff[0])
    diff[1] = scale(diff[1])
    set_ship_vector(diff[0], diff[1])
    time.sleep(.5)
    diff = array(dest_pos - ship_pos, dtype='f')
  set_ship_vector(0,0)

def escape_system(shipX, shipY):
 if abs(100 - shipX) < abs(100 - shipY):
   x = shipX
   y = 199 if shipY > 100 else 1
 else:
   x = 199 if shipX > 100 else 1
   y = shipY
 return array((x, y))

def go_to_edge():
  ship_pos = get_ship_pos()
  x = ship_pos[0]
  y = ship_pos[1]
  dest_pos = escape_system(x, y)
  #dest_pos = array((0,0))
  go_to_pos(dest_pos)

def go_to_planet(planets=None, dest=None):
  if planets == None:
      planets = get_planets()
  dest_pos = planets[dest]
  print "going to:", planets[dest]

  while get_current_planet() != dest:
    ship_pos  = get_ship_pos()
    print "ship pos", ship_pos
    diff = array(dest_pos - ship_pos, dtype='f')
    diff[0] = scale(diff[0])
    diff[1] = scale(diff[1])
    set_ship_vector(diff[0], diff[1])
    time.sleep(.5)
  set_ship_vector(0,0)

def main():
  dest = None
  if len(sys.argv) > 1:
    dest = sys.argv[1]
  planets = get_planets()
  ship_pos = get_ship_pos()
  print "ship pos", ship_pos

  set_ship_vector(0,0)

  if dest == None:
    plot_map(planets, ship_pos)
    dest = get_dest()

  if dest == 'edge':
    go_to_edge()
  else:
    go_to_planet(planets, dest)

  set_ship_vector(0,0)
  ship_pos = get_ship_pos()
  print "ship pos", ship_pos

if __name__ == '__main__':
    main()

