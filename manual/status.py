#!/usr/bin/python

import pprint
from lib import api

pprint.pprint(api('ship', 'show'))
pprint.pprint(api('ship', 'showresources'))
pprint.pprint(api('ship', 'showsystems'))
pprint.pprint(api('longrange'))
pprint.pprint(api('shortrange'))
pprint.pprint(api('object'))
