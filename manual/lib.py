import json
import sys
from pprint import pprint
import numpy as np
sys.path.append("../external/requests")
import requests

def api(command, *args):
  parms = '&'.join('arg%s=%s' % (str(i+1) if i else '', val) for i, val in enumerate(args))
  if parms:
    parms = '&'+parms
  url = "https://64.129.254.38:8000/api/?session=69211b38-eb60-48b4-93ac-8cf93b0c2129&command=%s%s" % (command, parms)
  #print url
  r = requests.get(url, verify=False)
  return json.loads(r.content)

def get_ship_pos(coords='system'):
  result = api('ship', 'show')
  x = result[coords+'x']
  y = result[coords+'y']
  return np.array((x, y))

def current_resources():
  resources = {}
  for item in api('ship', 'showresources')['resources']:
    resources[item['name']] = item['amount']
  return resources

def cur_planet():
  return api('object')['object_data']

def cur_system():
  result = api('ship', 'show')
  return result['currentsystem']

def mine():
  while True:
    mined = False
    for name, val in current_resources().items():
      if val <= 9.5:
        ret = api('ship', 'drones', name)
        pprint(ret)
        if 'error' not in ret:
          if 'interrupted' not in ret['mining_data']:
            mined = True
    if not mined:
      break


def closest_system(systems):
  pos = get_ship_pos('uni')
  def dist_from_me(system):
    spos = np.array((system['x'], system['y']), dtype='f')
    return np.linalg.norm(spos - pos)
  return sorted(systems.values(), key=dist_from_me)[0]


def closest_planet(planets):
  pos = get_ship_pos('system')
  def dist_from_me(planet):
    spos = np.array((planet['x'], planet['y']), dtype='f')
    return np.linalg.norm(spos - pos)
  return sorted(planets, key=dist_from_me)[0]


KNOWN_UNOBTANIUM = set([
    'Epsilon Eridani I'
])

def has_resource(resource, planet):
  if planet['minor_moons'] == 'minor_moons' and resource == 'Xenium':
    return True
  if float(planet['volatile_gas_inventory']) and resource == 'Vespine gas':
    return True
  if float(planet['hydrosphere']) > 0.01 and resource == 'Tiberium':
    return True
  if planet['planet_no'] in KNOWN_UNOBTANIUM and resource == 'Unobtanium':
    return True
  return False

