#!/usr/bin/python

import json
import os
import pprint
import sys
import time

from lib import api

import matplotlib
import matplotlib.pyplot as plt
from numpy import array

from nav_stars import go_to_star, get_stars
from nav_planet import go_to_planet, go_to_edge
from lib import mine
from lib import cur_system, closest_planet
from find_for_resource import closest_system_with_resource

def main(argv):
  dest = None
  if len(argv) > 1:
    resource = argv[1]

  print "Searching for resource: ", resource

  system = closest_system_with_resource(resource)
  if (cur_system() != system[u'name']):
    go_to_edge()
    go_to_star(get_stars(), system['name'])
  planet = closest_planet(system['planets'])
  go_to_planet(dest=planet['planet_no'])
  mine()

if __name__ == '__main__':
    main(sys.argv)

