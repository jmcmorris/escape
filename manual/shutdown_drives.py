#!/usr/bin/python

import json
import os
import pprint
import sys
import time
from lib import api

import matplotlib
import matplotlib.pyplot as plt
from numpy import array

def get_ship_uni_pos():
  result = api('ship', 'show')
  x = result['unix']
  y = result['uniy']
  return array((x, y))

def set_ship_uni_vector(x, y):
  api('ship', 'setunivecx', x)
  api('ship', 'setunivecy', y)
  print "setting uni_vector to: ", x, y

def get_ship_planet_pos():
  result = api('ship', 'show')
  x = result['systemx']
  y = result['systemy']
  return array((x, y))

def set_ship_planet_vector(x, y):
  api('ship', 'setsystemvecx', x)
  api('ship', 'setsystemvecy', y)
  print "setting planet_vector to: ", x, y
def scale(num, limit=0.4):
  sign = 1 if num >= 0 else -1
  num = min(abs(num), limit)
  return num * sign

set_ship_uni_vector(0,0)
set_ship_planet_vector(0,0)
ship_pos = get_ship_uni_pos()
print "ship uni pos", ship_pos
ship_pos = get_ship_planet_pos()
print "ship planet pos", ship_pos

