#!/usr/bin/python

import colorama
colorama.init()

from colorama import Fore

import json
import sys
import os
from pprint import pprint
from lib import api, has_resource

planet_stats = api('object')['object_data']
print "Planet data:"
for key, val in planet_stats.items():
    spaces = " "*(30-len(key))
    print '%s:%s%s' % (key, spaces, val)

something = False
print
if has_resource('Xenium', planet_stats):
    something = True
    print Fore.BLUE + 'Chance of Xenium!'
if has_resource('Vespene gas', planet_stats):
    something = True
    print Fore.GREEN +'Chance of Vespene gas (%s)!' % planet_stats['volatile_gas_inventory']
if has_resource('Tiberium', planet_stats):
    something = True
    print Fore.YELLOW + 'Chance of Tiberium (%s)!' % planet_stats['hydrosphere']
if has_resource('Unobtanium', planet_stats):
    something = True
    print Fore.MAGENTA + 'Chance of Unobtanium!'

if not something:
    print Fore.RED + 'No resources at this planet :('

if len(sys.argv) > 1:
    print 'Going to dump!'
    import cPickle as pickle

    data = {}
    if os.path.exists('planet.db'):
        with open('planet.db', 'rb') as f:
            data = pickle.load(f)

    data[planet_stats['planet_no']] = planet_stats

    with open('planet.db', 'wb') as f:
        pickle.dump(data, f)
