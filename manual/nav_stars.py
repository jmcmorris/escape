#!/usr/bin/python

import json
import os
import pprint
import sys
import time
from lib import api

import matplotlib
import matplotlib.pyplot as plt
from numpy import array

def get_ship_pos():
  result = api('ship', 'show')
  x = result['unix']
  y = result['uniy']
  return array((x, y))

def get_current_system():
  result = api('ship', 'show')
  return result['currentsystem']

def get_stars():
  stars = {}
  result = api('longrange')
  print result
  for star in result['stars']:
    x = int(star['x'])
    y = int(star['y'])
    name = star['name']
    stars[name] = array((x, y))
  return stars

def get_dest():
  print "Enter destination system."
  dest = sys.stdin.readline()
  print "dest is ", dest
  return dest.strip()

def set_ship_vector(x, y):
  api('ship', 'setunivecx', x)
  api('ship', 'setunivecy', y)
  print "setting vector to: ", x, y

def plot_map(stars, ship_pos):
  matplotlib.rcParams['axes.unicode_minus'] = False
  fig = plt.figure()

  ax = fig.add_subplot(111)
  ax.set_title('Long Range Scanner')
  for name,pos in stars.items():
    x = int(pos[0])
    y = int(pos[1])
    ax.plot(x, y, 'ro')
    plt.annotate(name, xy=(x, y), xytext=(x, y), arrowprops=dict(facecolor='black', shrink=0.05))
  ax.plot(ship_pos[0], ship_pos[1], 'o')
  plt.show()

def scale(num, limit=1):
  sign = 1 if num >= 0 else -1
  num = min(abs(num), limit)
  return num * sign

def go_to_star(stars, dest):
  dest_pos = stars[dest]
  print "going to:", stars[dest]

  while get_current_system() != dest:
    ship_pos  = get_ship_pos()
    print "ship pos", ship_pos
    diff = array(dest_pos - ship_pos, dtype='f')
    diff[0] = scale(diff[0])
    diff[1] = scale(diff[1])
    set_ship_vector(diff[0], diff[1])
    time.sleep(.5)
  set_ship_vector(0,0)

def main():
  dest = None
  if len(sys.argv) > 1:
    dest = sys.argv[1]

  print "going to star: ", dest

  stars = get_stars()
  ship_pos = get_ship_pos()
  print "ship pos", ship_pos

  set_ship_vector(0,0)

  if dest == None:
    plot_map(stars, ship_pos)
    dest = get_dest()

  go_to_star(stars, dest)

  set_ship_vector(0,0)
  ship_pos = get_ship_pos()
  print "ship pos", ship_pos

if __name__ == '__main__':
    main()

