#!/usr/bin/python

from lib import api
import json
import sys
import time
import pprint
import colorama
import autopilot
colorama.init()

Fore = colorama.Fore

to_repair = sys.argv[1] if len(sys.argv) > 1 else None

auto = False
if to_repair == 'auto':
  auto = True
  to_repair = sys.argv[2]

print "Planning to repair", to_repair
if auto:
    print "Automatically!"

fab_workshop = {
    "Alcubierre Singularity Propulsion Drive": { "Unobtainium": 2},
    "Manifold Tidal Stabilizer": { "Vespene gas": 2},
    "Repulsor Shields": { "Xenium": 2},
    "Directed Energy Weapon System": {"Unobtainium": 2},
    "Mining Drones": { "Tiberium": 6},
    "Tactical Conflict AI": { "Unobtainium": 2, "Tiberium": 4},
    "Gravitational Disturbance Sensor": { "Vespene gas": 2},
    "Sublight Impulse Propulsion Drive": { "Xenium": 2},
    "Devirtualization Fab Workshop": { "Tiberium": 6}
}

resources = {}
for item in api('ship', 'showresources')['resources']:
    resources[item['name']] = item['amount']

print 'We have:',resources

health = {}
for item in api('ship', 'showsystems')['systems']:
    health[item['name']] = item['functionality']

def can_repair(reqs):
    for resource_name, val in reqs.items():
        if resources[resource_name] < val:
            return False
    return True

def needed(reqs):
  ret = {}
  for name, val in reqs.items():
    have = resources[name]
    if resources[name] < val:
      ret[name] = val - have
  return ret

for name, health_val in health.items():
  if health_val < 1:
    reqs = fab_workshop[name]
    name_health = "%s (%s)" % (name, health_val)
    spaces = " "*(50-len(name_health))
    if can_repair(reqs):
        print '%s:%s req:  %s' % (name_health, spaces, reqs)
        if name == to_repair:
            rep = '  Repairing'
            spaces = " "*(52-len(rep))
            print '%s:%s%s' % (rep, spaces, api('ship', 'dorepair', name))
    else:
        need = needed(reqs)
        print '%s:%s req:  %s' % (name_health, spaces, reqs)
        print " "*52+Fore.RED+('need: %s' % (need,))+Fore.RESET
        if auto:
            print 'engaging autopilot'
            for resource in need.keys():
              import pdb; pdb.set_trace()
              autopilot.main(['bogus', resource])
            api('ship', 'dorepair', name)
            print 'done'
    print
  else:
    spaces = " "*(51-len(name))
    print '%s:%s%s' % (name, spaces, Fore.GREEN + 'OK' + Fore.RESET)

