#!/usr/bin/env python

import sys
import numpy as np
from pprint import pprint
import cPickle as pickle
from lib import api, get_ship_pos,closest_system
from lib import has_resource

def try_float(x):
    try:
        return float(x)
    except ValueError:
        return x


def closest_system_with_resource(resource):
  with open('planet.db', 'rb') as f:
      data = pickle.load(f)

  candidate_systems = {}
  stars = api('longrange')['stars']
  for star in stars:
    for planet in data.values():
      if planet['planet_no'].startswith(star['name']):
        if has_resource(resource, planet):
          if not star['name'] in candidate_systems:
            candidate_systems[star['name']] = star
            candidate_systems[star['name']]['planets'] = []
          candidate_systems[star['name']]['planets'].append(planet)

  ret = closest_system(candidate_systems)
  pprint(ret)
  return ret

if __name__ == '__main__':
  closest_system_with_resource(sys.argv[1])

