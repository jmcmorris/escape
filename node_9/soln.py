#!/usr/bin/python

import json
import os
import pprint
import sys

SCRIPT_PATH = lambda *pieces: os.path.abspath(os.path.join(os.path.dirname(__file__), *pieces))
sys.path.append(SCRIPT_PATH("..", "external", "requests"))
sys.path.append(SCRIPT_PATH("..", "external", "matplotlib", "lib"))
sys.path.append(SCRIPT_PATH("..", "external", "dateutil"))
sys.path.append(SCRIPT_PATH("..", "external", "pyparsing"))

import requests
import matplotlib
import matplotlib.pyplot as plt

r = requests.get("https://64.129.254.38:8000/api/?session=399cbcd7-7ddc-4495-b9f3-de5343927256&command=shortrange", verify=False)

matplotlib.rcParams['axes.unicode_minus'] = False
fig = plt.figure()

ax = fig.add_subplot(111)
ax.set_title('Short Range Scanner')
result = json.loads(r.content)
pprint.pprint(result)
for planet in result['system']['planetarray']:
  x = int(planet['x'])
  y = int(planet['y'])
  ax.plot([x], [y], 'ro')
  plt.annotate(planet['planet_no'], xy=(x, y), xytext=(x, y), arrowprops=dict(facecolor='black', shrink=0.05))

r = requests.get("https://64.129.254.38:8000/api/?session=399cbcd7-7ddc-4495-b9f3-de5343927256&command=ship&arg=show", verify=False)
result = json.loads(r.content)
x = result['systemx']
x = result['systemy']
ax.plot([x], [y], 'o')
plt.annotate('Ship', xy=(x, y), xytext=(x, y), arrowprops=dict(facecolor='black', shrink=0.05))

ax.plot(100, 100, 'o')
plt.annotate('STAR', xy=(100, 100), xytext=(100, 100), arrowprops=dict(facecolor='black', shrink=0.05))

plt.show()
